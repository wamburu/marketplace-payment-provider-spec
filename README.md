A specification of how to integrate on the backend for the Firefox Marketplace
payment providers.

Read the docs here: http://marketplace-payments-specification.readthedocs.org/
